var cl = console.log;
var lcd = require("HD44780").connect(B0,C2,C9,C8,C7,C6);
var crc8 = require("crc8").connect();
var unt = "cel";	// Units Name
var KalmanFilter = require("kalman").connect;
var filterHumIn = new KalmanFilter();
var filterHumOut = new KalmanFilter();

/*
var pinOW    = A3;	// DS18B20 pin
var pinHumIn = A4;	// Humidity In pin
var pinHumOt = A5;	// Humidity Out pin
var pinHeat = P5;   // Heater!!!! Нога лоя нагревателя.
var MySerial = Serial4;
var pinFan   = P2;	// Fan pin
*/

var pinOW    = A10;	// DS18B20 pin
var pinHumIn = A0;	// Humidity In pin
var pinHumOt = A1;	// Humidity Out pin
var pinHeat = C1;
var MySerial = Serial6;
var pinFan   = C0;	// Fan pin

var makeDes  = 25;	// Delay in seconds to check sensors and make decision to switch fan
var lagOn = 0.5;	// g/m3, Histeresis between humidityes to on fan
var lagOff = 0.1;	// g/m3, Histeresis between humidityes to off fan

var minInTemp = 2;	// Minimun inner temperature at cellar
var maxOutTemp = 12; // Maximum Out temperature when fan is switched ON

var param = {
	unt : "cel", 
	mod : "ON",
	fan : "OFF",
	faF : "OFF",	// Fan is forced switched 
	hit : "OFF",	// Heating
	hiF : "OFF",    // Heating is forced switched
	hiM : 5.5,		// Heating Max temperature
	tg  : 4,		// Target temperature at cellar
	tIn : 16.4,		// Temperature at Cellar
	hIn : 95.3,		// Humidity at Cellar
	tOt : 10.8,		// Outside Temperature
	hOt : 20.4,		// Outside Humidity
};

/************************  Just Switch The Fan  ************************/
function SwitchFan(sw, wherefrom) {
	digitalWrite(pinFan, sw);
	if(sw) {param.fan = "ON";}
	else {param.fan = "OFF";}
cl("Fan is ", sw, wherefrom);
	// SendData("fan:"+param.fan);
}
/************************  Just Switch The Heating *********************/
function SwitchHeat(sw) {
	digitalWrite(pinHeat, sw);
	if(sw) {param.hit = "ON";}
	else {param.hit = "OFF";}
cl("Hit is ", sw);
	// SendData("hit:"+param.hit);
}

/************************ Is It Time To Fan  ***************************/

//------------------------ Print LCD ----------------------------

function printLCD(aIn, aOut, inf) {
	if(inf) {
		lcd.clear();
		lcd.clear();
		lcd.setCursor(0,0);
		lcd.print("    Cellar 102");
		lcd.setCursor(0,1);
		lcd.print("     Jiaffe!");
	}
	else {
		lcd.clear();
		lcd.setCursor(0,0);
		lcd.print(param.tIn.toFixed(1)+"/"+param.hIn.toFixed(0)+"="+aIn.toFixed(0));
		lcd.setCursor(13,0);
		lcd.print("F:");
		if (param.fan === "ON"){
			lcd.print("\u00D9");
            x = true;
		}
		else {
			x = false;
			lcd.print("\u008D");
		}
		lcd.setCursor(13,1);
		lcd.print("M:");
		if (param.mod === "ON"){
			lcd.print("\u0094");
		}
		else {
			lcd.print("\u008D");
		}
		lcd.setCursor(0,1);
		lcd.print(param.tOt.toFixed(1)+"/"+param.hOt.toFixed(0)+"="+aOut.toFixed(0));
	}
}

var x = false;
var pr = ['+', '/','X', '+','\\','X'];
var nw = 0;

var c = setInterval(function(){
  if(x) {   
    lcd.setCursor(15,0);
    lcd.print(pr[nw]);
    nw++;
    if(nw === 6) nw = 0;
  }
}, 150);


//-------------------------  UART Init -------------------------------
//var MySerial = Serial6;
MySerial.setup(9600);
var getUART = false;
var inByte = "";
var command = "";

//-----------------------  UART Send --------------------------------

function SendData (dt)  {
	var d = JSON.stringify(dt);
    var codeddt = crc8.encode(d);
    MySerial.print(codeddt+";");
cl("Sent "+d.length+" bytes: ", codeddt+";");
}

function SlowSendData() {
	setTimeout(function(){
		SendData(param);
	}, 500);
}

//--------------------  UART Received -----------------------------------

// cl(crc8.encode('{"cel":1,"tg":7}'))
// Serial6.emit('data', '{"cel":0}139#29;');
// MySerial.emit('data', '{"cel":1,"faF":"ON"}44#189;');
// {"cel":1,"tg":7}199#105;
// {"cel":1,"mod":"OFF"}226#245;
// MySerial.emit('data', '{"mod":"OFF"}181#62;')
// is bad {"cel":1,"faF":"OFF"}44#189;
// is long {"cel":1,"faF":"OFF"}44#1897;


MySerial.on('data', function(data) {
    inByte = data;
	if (inByte.charCodeAt(0) === 123) { // "{"
		command = "";
		getUART = true;
	}
	if (getUART === true) {
      command += inByte;
      if(command.length > 55) {
        getUART = false;
      } 
      var stopNow = command.indexOf(';'); 
      if (stopNow > 0) { // ";"
        GetCom(command.substring(0,stopNow));
        getUART = false;
      }
	}
});

function GetCom (packet) {
	//var SwitchFan = SwitchFan;
    // cl("GetCom", packet);
    if (packet.indexOf(unt) === -1 ){
		//cl("Not My Comm!");
		return;
	}
	aa = crc8.decode(packet);
	//cl("crc8 Report = ", aa);
	if (aa.indexOf("CRC") > 0 ){
		return;
	}
	
	var dd = JSON.parse(aa);
	//cl(dd);
	if(dd[unt] === 0) {
		//cl("Ask Me a Data!");
		// Answer:
		SlowSendData();
		return;
	}
	
	if (dd.hasOwnProperty("hiF")) {
		//cl("Got hiF");
		if (dd.hiF === "ON") {
			param.hiF = "ON";
			SwitchHeat(1);
			}
		else {
			param.hiF = "OFF";
			SwitchHeat(0);
			}
	}
	
	if (dd.hasOwnProperty("faF")) {
		//cl("Got faF");
		if (dd.faF === "ON") {
			SwitchFan(1);
			param.mod = "OFF";
			param.faF = "ON";
		}
		else {
			SwitchFan(0);
			param.mod = "ON";
			param.faF = "OFF";
		}
	}
	if (dd.hasOwnProperty("mod")) {
		//cl("Got mod");
		if (dd.mod === "ON") {
			param.mod = "ON";
			param.faF = "OFF";
		}
		else {
			SwitchFan(0);
			param.mod = "OFF";
			param.faF = "OFF";
		}
	}

	if (dd.hasOwnProperty("tg")) {
		//cl("Got tg");
		param.tg = parseInt(dd.tg, 10);
	}
	if (dd.hasOwnProperty("hiM")) {
		//cl("Got hiF");
		param.hiM = parseInt(dd.hiM, 10);
	}
	//cl(param);
}


//------------------------  DS18B20 Init ---------------------------
/**/
var ow = new OneWire(pinOW);
try{
	sensor1 = require("DS18B20").connect(ow, 0);
} catch(e){
	cl("No sensor 1");
}

try{
	sensor2 = require("DS18B20").connect(ow, 1);
} catch(e){
	cl("No sensor 2");
}


/**/

//----------------------------  GetSensors ---------------------------
function GetSensors() {
	var GetHue = function (pin, temp) {
		var RH =  (analogRead(pin) - 0.1515)/ 0.00636;
		var TrueRH = (RH)/(1.0546 - 0.00216 * temp);
		var filteredHum = 0;
		if(pin === pinHumIn){ 
			filteredHum = filterHumIn.update(TrueRH);
		}
		else {
			filteredHum = filterHumOut.update(TrueRH);
		} 
		return parseFloat(filteredHum.toFixed(1));
	};

	function secondTemp(call) {
	  if (sensor2) {
		sensor2.getTemp(function (tOut) {
			cl("Temp is "+tOut.toFixed(1)+" C"); 
			param.tOt =  parseFloat(tOut.toFixed(1));
			var gOt = GetHue;
			param.hOt = gOt(pinHumOt, tOut);
			if (call) {call();}
		});
	  }
	  else{
		param.tOt = 85;
		param.hOt = 125;
		if (call) {call();}
	  }
	}
		
	function firstTemp(call, isFan) {
	  if(sensor1){
		sensor1.getTemp(function (tInn) {
			cl("Temp is "+tInn.toFixed(1)+" C"); 
			param.tIn = parseFloat(tInn.toFixed(1));
			var gin = GetHue;
			param.hIn = gin(pinHumIn, tInn);
			if (call) {call(isFan);}
		});
	  }
	  else{
		param.tIn = 85;
		param.hIn = 125;
		if (call) {call(isFan);}
	  }
	}
	firstTemp(secondTemp, isFan);

	return;
}


/************************ Calculate ABS Humi ***************************/

function CalcAbsH(t, h) { // t - temperature celsius, h - humidity %
	var tmp;
	var absHumid;
	tmp = Math.pow(2.718281828,(17.67*t)/(t+243.5));
	absHumid = (6.112*tmp*h*2.1674)/(273.15+t);
cl('Abs Humidity is ', absHumid.toFixed(2), "g/m3");
	return absHumid;
}


function isFan(){
	function runFan() {
		var aHumIn  = CalcAbsH(param.tIn, param.hIn);
		var aHumOut = CalcAbsH(param.tOt, param.hOt);
		var delta = aHumIn - aHumOut;
		//*************************** Fan ******************************
		if(param.mod == "ON") {
			if(( (param.tIn < param.tg) || (param.tIn < minInTemp) || (param.tOt > maxOutTemp)) && (param.fan === "ON")){  
				SwitchFan(0, "One");
			}
			
			else if ((param.tIn >  (param.tg + 1)) && (param.tIn > param.tOt) && (param.tIn < maxOutTemp)) {  	// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Changed 
				if ((delta > lagOn) && (param.fan === "OFF")) {
					SwitchFan(1, "Two");
				}
				if  ((delta < lagOff) && (param.fan === "ON")) {
					SwitchFan(0, "Three");
				}
			}
		}

		if(param.faF == "ON") {
			if((param.tIn < minInTemp) && (param.fan === "ON"))
				SwitchFan(0, "Foth");
				// param.mod = "ON";
				param.faF = "OFF";
		}
		
		//************************ heating ***************************
		if((param.hiF == "OFF")){
			if((param.tIn < (param.tg - 0.75)) && ((param.hit == "OFF"))) {
				SwitchHeat(1);
			}
			if((param.tIn > param.tg) && (param.hit == "ON")){
				SwitchHeat(0);
			}
		}
		
		if((param.tIn > param.hiM) && (param.hit == "ON")){
			SwitchHeat(0);
			param.hiF = "OFF";
		}
		//**************************************************************
		printLCD(aHumIn, aHumOut);
		// SlowSendData();
	}
	
	if (param.tIn !==85 && param.tOt!==85) {
		runFan();
	}
	else {
		cl("Sensor(s) lost! Demo mode!");
		param.tIn = Math.random() * 10;		// Temperature at Cellar
		param.hIn = Math.random() * 100;	// Humidity at Cellar
		param.tOt = Math.random() * 10;		// Outside Temperature
		param.hOt = Math.random() * 20;
		runFan();
	}
}

 // E.on('init', function() {
	setTimeout(function () {
		printLCD(1,1,true);
		GetSensors();
	}, 3000);
  
  var ddd = setInterval(function() { 
	GetSensors(true);
  }, makeDes*1000); 
  
// });