
var kalman = function(variance, varProcess) {
	this.variance = variance || 1.12184278324081/100000;
	this.varProcess = varProcess || 1/100000000;
	//console.log("variance = ", this.variance);
	//console.log("varProcess = ", this.varProcess);
	
	this.Pc = 0.0;
	this.G = 0.0;
	this.P = 1.0;
	this.Xp = 0.0;
	this.Zp = 0.0;
	this.Xe = 0.0;
} 
kalman.prototype.set = function(variance, varProcess){
	this.variance = variance || 1.12184278324081/100000;
	this.varProcess = varProcess || 1/100000000;
	//console.log("variance = ", this.variance);
	// console.log("varProcess = ", this.varProcess);
}

kalman.prototype.update = function(vol) {
	this.Pc = this.P + this.varProcess;
	this.G = this.Pc/(this.Pc + this.variance);
	this.P = (1-this.G)*this.Pc;
	this.Xp = this.Xe;
	this.Zp = this.Xp;
	this.Xe = this.G*(vol-this.Zp) + this.Xp;  
	return this.Xe;
}

exports.connect = function(variance, varProcess) {
    return new kalman(variance, varProcess);
};

